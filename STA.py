# spike triggered average
import numpy as np
from os import listdir

__all__=['Corem_firing','ReceptiveField2D','ReceptiveField3D', 'temporalSTA']

def Corem_firing(file, datalength=60000, binsize=10):
	Coremdata = np.load(file)
	binedge = np.linspace(0, datalength*binsize, num=datalength+1, endpoint=True)
	FiringRate = np.array([np.histogram(Coremdata.item()[i],bins = binedge)[0].astype(float) for i in Coremdata.item()])
	return FiringRate[2:627]

def ReceptiveField2D(FiringRate,allimage,datalength=60000, binsize=10):
	# FiringRate.shape = [channel,data], allimage.shape = [data,W,H]
	# ReceptiveField2D(binningrate.T,np.array(allimage))
	leng = min([datalength,FiringRate.T.shape[1]])
	RFallimage=[]
	for k in range(leng):
		j=FiringRate[k]
		spikeind = np.nonzero((j>np.std(j))*1)[0]
		RFimage = np.zeros([50,50])
		for i in spikeind:
			RFimage += allimage[i,:,:]
		RFallimage.append(RFimage/len(spikeind))
	return RFallimage


def ReceptiveField3D(FiringRate, npfolderpath=None, imgstack=None):
	# RFallimage = np.zeros((FiringRate.shape[0],40,50,50)) # for corem
	RFallimage = np.zeros((FiringRate.shape[0],40,49,49)) # for exp
	channelID = 0
	if (imgstack is None) and (npfolderpath is None):
		print("input npfolderpath or imgstack")
		npfolderpath=input("npfolderpath=")
	if npfolderpath is not None:
		print("npfolderpath")
		files = listdir(npfolderpath)
		files.sort()
		for j in FiringRate:
			spikeind = np.nonzero((j>np.std(j))*1)[0]+39
			for i in spikeind:
				for k in range(40):
					RFallimage[channelID,39-k,:,:] += np.load(npfolderpath+files[i-k])
			RFallimage[channelID,:,:,:] = RFallimage[channelID,:,:,:]/len(spikeind)
			channelID += 1   
	else:
		print("imgstack")
		imgstack = np.transpose(imgstack,axes=[0,3,1,2])
		for j in FiringRate:
			spikeind = np.nonzero((j>np.std(j))*1)[0]
			for i in spikeind:
				RFallimage[channelID,:,:,:] += imgstack[i]
			RFallimage[channelID,:,:,:] = RFallimage[channelID,:,:,:]/len(spikeind)
			channelID += 1
	return RFallimage


def temporalSTA(channel_data,signal, TimeStamps, sampling, pretime=1. , posttime=1.): # sampling unit: Hz ; others: s;
    allSTA = []
    for spikestamp in channel_data:
        prelen = int(pretime*sampling)
        postlen = int(posttime*sampling)        
        if 10 > spikestamp.shape[0]:
            allSTA.append(np.ones(prelen+postlen))
        else:
            sumSTA = np.zeros(prelen+postlen)
            for i in spikestamp: 
                if (pretime+TimeStamps[0][0])<i<(TimeStamps[0][-1]-posttime) :
                    sumSTA += signal[int(i*sampling)-prelen:int(i*sampling)+postlen]
            allSTA.append(sumSTA/spikestamp.shape[0])
    return allSTA





