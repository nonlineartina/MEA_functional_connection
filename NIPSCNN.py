# try to code the model of Deep learning models reveal internal structure and diverse computations in the retina under natural scenes
# Tina @ Jul 9, 2018
# https://www.biorxiv.org/content/early/2018/06/14/340943
# https://github.com/baccuslab/deep-retina/tree/master/deepretina

# try to code the model of Deep learning models reveal internal structure and diverse computations in the retina under natural scenes
# Tina @ Jul 9, 2018
# https://www.biorxiv.org/content/early/2018/06/14/340943
# https://github.com/baccuslab/deep-retina/tree/master/deepretina
import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import Model
from keras.layers.core import Dense, Activation, Flatten, ActivityRegularization
from keras.layers import Input, Conv2D, BatchNormalization,GaussianNoise
from keras.regularizers import l1, l2
from keras import backend as K
from keras.callbacks import EarlyStopping,ModelCheckpoint
from keras.models import load_model


__all__=[]



__all__ = ['Tina_CNN','Bioxive_CNN_2018','NIPS_CNN_2017','get_layer_output', 'get_kernel']
# def selu(x):
#     """Scaled Exponential Linear Unit (SELU).
# 	https://github.com/keras-team/keras/blob/master/keras/activations.py"""
#     alpha = 1.6732632423543772848170429916717
#     scale = 1.0507009873554804934193349852946
#     return scale * K.elu(x, alpha)

#  # loss function: negative log-likelihood
# from keras import backend as K
# def NegLogLikeLoss(y_true, y_pred):
# 	return K.mean(y_pred - y_true * K.log(y_pred), axis=-1)

def Tina_CNN(n_out):
	# keras model
	inputs = Input(shape=(50,50,40)) # Tensorflow is channel last format
	# layer 1, spatiotemporal covolution
	x = Conv2D(8, (15,15), kernel_regularizer=l2(0.01))(inputs) # (height, width)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 2, spatial convolution
	# x = K.squeeze(x, [3])
	x = Conv2D(8, (11,11), kernel_regularizer=l2(0.01))(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 3, all connection	
	x = Flatten()(x)
	x = Dense(n_out, use_bias=False)(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	outputs = Activation('softplus')(x)
	# This creates a model
	return Model(inputs, outputs, name='Tina_CNN')

def Bioxive_CNN_2018(n_out):
# keras model
	inputs = Input(shape=(50,50,40)) # Tensorflow is channel last format
	# layer 1, spatiotemporal covolution
	x = Conv2D(8, (15,15), kernel_regularizer=l2(0.01))(inputs) # (height, width)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 2, spatial convolution
	# x = K.squeeze(x, [3])
	x = Conv2D(8, (11,11), kernel_regularizer=l2(0.01))(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 3, all connection	
	x = Flatten()(x)
	x = Dense(n_out, use_bias=False)(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	outputs = Activation('softplus')(x)
	# This creates a model
	return Model(inputs, outputs, name='Bioxive_CNN_2018')

def NIPS_CNN_2017(n_out):
# keras model
	inputs = Input(shape=(50,50,40)) # Tensorflow is channel last format
	# layer 1, spatiotemporal covolution
	x = Conv2D(8, (15,15), kernel_regularizer=l2(0.01))(inputs) # (height, width)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 2, spatial convolution
	x = Conv2D(16, (15,15), kernel_regularizer=l2(0.01))(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	x = Activation('relu')(x)
	# layer 3, all connection	
	x = Flatten()(x)
	x = Dense(n_out, use_bias=False, kernel_regularizer=l2(0.001), activity_regularizer=l1(0.001))(x)
	x = BatchNormalization()(x)
	x = GaussianNoise(0.05)(x)
	outputs = Activation('softplus')(x)
	# This creates a model
	return Model(inputs, outputs, name='NIPS_CNN_2017')

def get_layer_output(model, model_inputs, outputlayer=3, training_flag=False, plot_flag=False, batchnum=0, kernelnum=0):
	get_output = K.function([model.layers[0].input, K.learning_phase()],[model.layers[outputlayer].output])
	layer_output = get_output([model_inputs, training_flag])[0]
	if plot_flag:
		plt.imshow(outs[batchnum,:,:,kernelnum])
		plt.show()
	return layer_output
# outputs = [layer.output for layer in model.layers]          # all layer outputs
# functors = [K.function([inp]+ [K.learning_phase()], [out]) for out in outputs] # evaluation functions

def get_kernel(model, layerID=0, channelID=0, kernelID=0):
	modelconfig=model.get_config()
	modelweight=model.get_weights()
	tempkernel = np.squeeze(modelweight[layerID][:,:,channelID,kernelID])
	return tempkernel



# n_out = targetspikes.shape[1]
# model = Tina_CNN(n_out)
# model.summary()
# adam_tune=keras.optimizers.Adam(lr=0.01)
# model.compile(loss='poisson', optimizer=adam_tune)
# mcpvalloss_save = ModelCheckpoint("83Bestvalloss20180805c.h5", save_best_only=True, monitor='val_loss', mode='min')
# mcploss_save = ModelCheckpoint("83Bestloss20180805c.h5", save_best_only=True, monitor='loss', mode='min')
# model.fit(imgstack, targetspikes, 
#           epochs=10, batch_size=2048,
#           validation_split=0.05,verbose=1, 
#           callbacks=[mcploss_save,mcpvalloss_save])
# pred = model.predict(imgstack)



